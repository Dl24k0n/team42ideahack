import serverResponse from '../mockdata/serverResponse.json'

export default class StargazeService {
    constructor() {
        if(this.occupations === undefined || this.occupations === null || this.occupations === [] || this.occupationSkillMatching === []) {
            this.serverResponse = serverResponse.response
            this.occupations = [];
            this.occupationSkillMatching = [];
            this.skills = [];
            this.enhanceServerResponseData()
        }
    }

    enhanceServerResponseData() {
        this.serverResponse.forEach((elem) => {
            if(!this.occupations.includes(elem.occupations_conceptURI)) {
                this.occupations.push(elem.occupations_conceptURI);
                this.occupationSkillMatching.push(this.initializeOccupationWithSkillArray(elem));
            } else {
                let index = this.occupationSkillMatching.findIndex((searchElem) => {
                    return searchElem.occupations_conceptURI === elem.occupations_conceptURI
                })
                this.occupationSkillMatching[index].skills.push(this.getOccupationSkill(elem))

            }
        })
    }

    capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    initializeOccupationWithSkillArray(elemToConvert) {
        let skillArray = [this.getOccupationSkill(elemToConvert)];
        const capitalized_occupation_name = elemToConvert.occupations_name.split(' ').map(this.capitalize).join(' ');
        return {
            occupations_conceptURI: elemToConvert.occupations_conceptURI,
            occupations_name: capitalized_occupation_name,
            occupations_description: elemToConvert.occupations_description,
            skills: skillArray,
            employeeMatch: 0,
            occupationMatch: 0,
            averageTwoWayFit: 0,
            employeeMissingSkills: [],
            employeeFittingSkills: []
        }
    }

    getOccupationSkill(elemToConvert) {
        let skill = {
            relationType: elemToConvert.relationType,
            skillType: elemToConvert.skillType,
            skills_conceptURI: elemToConvert.skills_conceptURI,
            skills_name: elemToConvert.skills_name,
            skills_description: elemToConvert.skills_description
        };
        this.skills.push(skill);
        return skill;
    }

    getPersonalSkillMatchingForOccupations(skillArray) {
        let employeeSkillArray = [];
        skillArray.forEach((skillElem) => {
            let elemToAdd = this.skills.find((searchElem) => {
                return skillElem === searchElem.skills_name;
            });
            if(elemToAdd !== undefined && elemToAdd != null) {
                employeeSkillArray.push(elemToAdd);
            }
        })

        this.occupationSkillMatching.forEach((occupationToMatch) => {
            this.calculateEmployeeFit(occupationToMatch, employeeSkillArray);
        })

        this.sortAndFilterOccupationsByEmployeeFit();
        console.log(this.occupationSkillMatching);
        return this.occupationSkillMatching;
    }

    calculateEmployeeFit(occupation, employeeSkillArray) {
        let calculatedEmployeeFit = 0;
        let calculatedOccupationFit = 0;
        let fitPercentagePerSkillOccupation = 1 / occupation.skills.length;
        let fitPercentagePerSkillEmployee = 1 / employeeSkillArray.length;
        occupation.skills.forEach((occupationSkill) => {
            if(occupationSkill.relationType === "essential") {
                if(employeeSkillArray.find((employeeSkill) => { return employeeSkill.skills_conceptURI === occupationSkill.skills_conceptURI})) {
                    calculatedEmployeeFit += fitPercentagePerSkillOccupation*1.4;
                    calculatedOccupationFit += fitPercentagePerSkillEmployee*1.4;
                    occupation.employeeFittingSkills.push(occupationSkill);
                } else {
                    occupation.employeeMissingSkills.push(occupationSkill);
                }
            } else if(occupationSkill.relationType === "optional") {
                if(employeeSkillArray.find((employeeSkill) => { return employeeSkill.skills_conceptURI === occupationSkill.skills_conceptURI})) {
                    calculatedEmployeeFit += fitPercentagePerSkillOccupation*0.8;
                    calculatedOccupationFit += fitPercentagePerSkillEmployee*0.8;
                    occupation.employeeFittingSkills.push(occupationSkill);
                } else {
                    occupation.employeeMissingSkills.push(occupationSkill);
                }
            }
        })
        occupation.employeeMatch = calculatedEmployeeFit;
        occupation.occupationMatch = calculatedOccupationFit;
        occupation.averageTwoWayFit = (calculatedOccupationFit*1.3 + calculatedEmployeeFit*0.7) / 2
    }

    sortAndFilterOccupationsByEmployeeFit() {
        this.occupationSkillMatching = this.occupationSkillMatching.filter((filterElem) => filterElem.averageTwoWayFit >= 0.05)
        this.occupationSkillMatching.sort((a, b) => {
            if (a.averageTwoWayFit < b.averageTwoWayFit) {
                return 1;
            }
            if (a.averageTwoWayFit > b.averageTwoWayFit) {
                return -1;
            }
            return 0;
        })
    }
}

/*
Data Structure explanation:

occupations: Array of all occupation concept URIs (Meaning all unique applications that are open in the company)
skills: Array of all skills that occur anywhere in the listing
occupationSkillMatching: The Heart and Soul; An Array of all occupations open in the company enriched with all the data a data scientist can dream of

occupationSkillMatching: [{
            occupations_conceptURI: Concept URI (Unique Identifier) of the occupation,
            occupations_name: Cleartext name of the occupation,
            occupations_description: Cleartext description of the occupation,
            skills: An array of all skills deemed essential or optional for the occupation (more about the structure of a skill below)
            employeeMatch: How well the employee matches the occupation, i.e. how many of the listed skills he covers (value in percentage)
            occupationMatch: How well the occupation matches the employee, i.e. how many of the employees skills are covered by the occupation (value in percentage)
            averageTwoWayFit: A weighted average between the employeeMatch and the occupationMatch (value in percentage)
            employeeMissingSkills: An array of all the skills the employee is missing in order to be a perfect match for the occupation,
            employeeFittingSkills: A list of all the skills that already make the employee a good fit for the position
}]

skill: {
            relationType: an indication about the importance of the skill for the occupation
            skillType: a classification of the type of skill, i.e. a knowledge of some kind or an actual skill that the employee must have developed for the position,
            skills_conceptURI: Concept URI (Unique Identifier) of the occupation,
            skills_name: The cleartext name of the skill,
            skills_description: Cleartext description of the skill
}
 */
