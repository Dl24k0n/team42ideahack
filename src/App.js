import React from 'react';
import './App.scss';
import LoginPage from "./pages/LoginPage/LoginPage";
import ProfilePage from "./pages/ProfilePage/ProfilePage";
import SuggestionsPage from "./pages/SuggestionsPage/SuggestionsPage";
import {HashRouter, Route} from "react-router-dom";
import {Redirect} from "react-router";

export default class App extends React.Component {

  render() {
    return (
        <HashRouter>
            <Route path="/login" component={LoginPage}/>
            <Route path="/profile" component={ProfilePage}/>
            <Route path="/suggestions" component={SuggestionsPage}/>
            <Route exact path="/">
                <Redirect to="/login" />
            </Route>
        </HashRouter>
    );
  }
}
