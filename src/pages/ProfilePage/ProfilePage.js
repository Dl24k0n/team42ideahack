import React from "react";
import './ProfilePage.scss';
import {Redirect} from "react-router";

export default class ProfilePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoadingData: false,
            loadingInterval: '',
            redirectToSuggestions: false
        }
        this.redirectToSuggestions = this.redirectToSuggestions.bind(this);
        this.startLoading = this.startLoading.bind(this);
        this.finishLoading = this.finishLoading.bind(this);
    }

    componentDidMount() {
        this.startLoading();
    }

    redirectToSuggestions() {
        this.setState({redirectToSuggestions: true})
    }

    startLoading() {
        this.setState({isLoadingData: true, loadingInterval: setInterval(this.finishLoading, Math.floor((Math.random() * 1000) + 500))});
    }

    finishLoading() {
        clearInterval(this.state.loadingInterval);
        this.setState({isLoadingData: false, loadingInterval: ''})
    }

    render() {
        if(this.state.isLoadingData) {
            return(
                <div>
                    Loading
                </div>
            )
        }
        if(this.state.redirectToSuggestions) {
            return <Redirect push to="/suggestions"/>
        }
        return(
            <div>
                <div>Hello David!</div>
                <div>The Skills Starmind has determined for you:</div>
                <ul>
                    <li>Really damn nice guy</li>
                    <li>like really nice</li>
                    <li>also incredibly smart</li>
                    <li>pretty levelheaded</li>
                    <li>keeps calm in stressful situations</li>
                </ul>
                <button onClick={this.redirectToSuggestions}>Search matching profiles for me</button>
            </div>
        )
    }
}
