import React from "react";
import './SuggestionsPage.scss';
import Modal from 'react-modal';
import Navbar from 'react-bootstrap/Navbar';
import starGazeLogo from '../../assets/stargazeLogoCut.png';
import profilePic from '../../assets/profilepic.png';
import loader from '../../assets/loader.gif';
import chevDown from '../../assets/arrow-down.svg';
import modalClose from '../../assets/clear.svg'
import StargazeService from "../../services/StargazeService";
import team1 from '../../assets/team1.jpeg';
import team2 from '../../assets/team2.jpeg';
import team3 from '../../assets/team3.jpeg';
import team4 from '../../assets/team4.jpeg';
import team5 from '../../assets/team5.jpeg';
import team6 from '../../assets/team6.jpeg';
import hrManager from '../../assets/hrmanager.jpg';
import phone from '../../assets/phone-icon.png';
import envelope from '../../assets/envelope-icon.png';
import checkIcon from '../../assets/check-icon.png';
import missingIcon from '../../assets/missing-icon.png';

export default class SuggestionsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
			isLoadingData: false,
			loadingInterval: '',
			modalIsOpen: false,
			personalSkillMatchingData: [],
			chosenSuggestion: {employeeFittingSkills: [], employeeMissingSkills: []}
		}
		this.stargazeService = new StargazeService();
        this.openOrCloseModal = this.openOrCloseModal.bind(this);
        this.referToTeachingSite = this.referToTeachingSite.bind(this);
        this.startLoading = this.startLoading.bind(this);
        this.finishLoading = this.finishLoading.bind(this);
    }
    componentDidMount() {
        this.startLoading();
    }
    openOrCloseModal(chosenSuggestion) {
        this.setState({modalIsOpen: !this.state.modalIsOpen, chosenSuggestion: chosenSuggestion})
    }
    referToTeachingSite(url) {
		const newUrl = url.replace(" ", "+");
		window.open('https://www.udemy.com/courses/search/?src=ukw&q='+newUrl, "_blank");
    }
    startLoading() {
        this.setState({isLoadingData: true, loadingInterval: setInterval(this.finishLoading, Math.floor((Math.random() * 1000) + 1500))});
		let serviceRes = this.stargazeService.getPersonalSkillMatchingForOccupations(["measure software usability", "debug software", "decision support systems", "apply statistical analysis techniques", "organise library material", "conduct ICT code review", "MDX", "ICT performance analysis methods", "LINQ", "use scripting programming", "sell flowers", "N1QL", "XQuery", "Process-based management", "liquidity management", "visual presentation techniques", "set quality assurance objectives", "report test findings", "plan software testing", "develop ICT test suite", "provide software testing documentation", "levels of software testing", "execute software tests", "address problems critically", "replicate customer software issues"])
		this.setState({personalSkillMatchingData: serviceRes})
		console.log(this.state.personalSkillMatchingData);
    }
    finishLoading() {
        clearInterval(this.state.loadingInterval);
        this.setState({isLoadingData: false, loadingInterval: ''})
    }
	round5(x)
	{
		return Math.floor(x/5)*5;
	}
    getProperMatchCSS(percentage) {
    	switch (this.round5(Math.round(percentage *100))) {
			case 100:
				return "innerPercentage p1"
			case 95:
				return "innerPercentage p2"
			case 90:
				return "innerPercentage p3"
			case 85:
				return "innerPercentage p4"
			case 80:
				return "innerPercentage p5"
			case 75:
				return "innerPercentage p6"
			case 70:
				return "innerPercentage p7"
			case 65:
				return "innerPercentage p8"
			case 60:
				return "innerPercentage p9"
			case 55:
				return "innerPercentage p10"
			case 50:
				return "innerPercentage p11"
			case 45:
				return "innerPercentage p12"
			case 40:
				return "innerPercentage p13"
			case 35:
				return "innerPercentage p14"
			case 30:
				return "innerPercentage p15"
			case 25:
				return "innerPercentage p16"
			case 20:
				return "innerPercentage p17"
			case 15:
				return "innerPercentage p18"
			case 10:
				return "innerPercentage p19"
			case 5:
				return "innerPercentage p20"
			default :
				return "innerPercentage p21"
		}
	}
    render() {
        if(this.state.isLoadingData) {
            return(
                <div className="loaderBox">
					<img src={loader} className="loaderGif" alt="loading"/>
                </div>
            )
		}
        return(
            <div>
				<Modal 
					isOpen={this.state.modalIsOpen}
					contentLabel="Job Info"
					className="modalStyle"
				>
					<div className="ModalName">
						{this.state.chosenSuggestion.occupations_name}
						<img
							src={modalClose}
							className="modalCloseButton"
							alt="modal close"
							onClick={() => this.openOrCloseModal({employeeFittingSkills: [], employeeMissingSkills: []})}
						/>
					</div>
					<div className="modalJobDescription">
						<h3>Description</h3>
						<span>
							{this.state.chosenSuggestion.occupations_description}
						</span>
					</div>
					<div className="outerPercentage modalPercentage">
						<div className={this.getProperMatchCSS(this.state.chosenSuggestion.averageTwoWayFit)}>
							<div>{Math.round(this.state.chosenSuggestion.averageTwoWayFit*100)}%</div>
						</div>
					</div>
					<div className="skillMatches">
						<div className="essentialSkillBox">
							<h3>Essential Skills</h3>
							<div className="flex-wrapper">
								{
									this.state.chosenSuggestion.employeeFittingSkills.filter((skill) => skill.relationType === "essential").map((fittingSkill) =>
										<div className="fitting-essential-skill" key={fittingSkill.skills_conceptURI}><img src={checkIcon} className="checkIcon" alt="checkbox icon"/>{fittingSkill.skills_name}</div>
									)
								}
								{
									this.state.chosenSuggestion.employeeMissingSkills.filter((skill) => skill.relationType === "essential").map(fittingSkill =>
										<div className="missing-essential-skill" onClick={() => this.referToTeachingSite(fittingSkill.skills_name)} key={fittingSkill.skills_conceptURI}><img src={missingIcon} className="missingIcon" alt="missing icon"/>{fittingSkill.skills_name}</div>
									)
								}
							</div>
						</div>
						<div className="optionalSkillBox">
							<h3>Optional Skills</h3>
							<div className="flex-wrapper">
								{
									this.state.chosenSuggestion.employeeFittingSkills.filter((skill) => skill.relationType === "optional").map(fittingSkill =>
										<div className="fitting-optional-skill" key={fittingSkill.skills_conceptURI}><img src={checkIcon} className="checkIcon" alt="checkbox icon"/>{fittingSkill.skills_name}</div>
									)

								}
								{
									this.state.chosenSuggestion.employeeMissingSkills.filter((skill) => skill.relationType === "optional").map(fittingSkill =>
										<div className="missing-optional-skill" onClick={() => this.referToTeachingSite(fittingSkill.skills_name)} key={fittingSkill.skills_conceptURI}><img src={missingIcon} className="missingIcon" alt="missing icon"/>{fittingSkill.skills_name}</div>
									)
								}
							</div>
						</div>
					</div>
					<div className="teamMeet">
						<h3>Meet the Team</h3>
						<div className="flexBox">
							<div className="quarter-div center">
								<img
									src={team1}
									className="teamPic"
									alt="team pic"
								/>
								<p>Tom Kirkland</p>
							</div>
							<div className="quarter-div center">
								<img
									src={team2}
									className="teamPic"
									alt="teamp pic"
								/>
								<p>Oriana Kumar</p>
							</div>
							<div className="quarter-div center">
								<img
									src={team3}
									className="teamPic"
									alt="team pic"
								/>
								<p>Vivian Navarro</p>
							</div>
							<div className="quarter-div center">
								<img
									src={team4}
									className="teamPic"
									alt="team pic"
								/>
								<p>Jean-Luc Mccullough</p>
							</div>
						</div>
						<div className="flexBox">
							<div className="quarter-div center">
								
							</div>
							<div className="quarter-div center">
								<img
									src={team5}
									className="teamPic"
									alt="team pic"
								/>
								<p className="teamName">Holli Byers</p>
							</div>
							<div className="quarter-div center">
								<img
									src={team6}
									className="teamPic"
									alt="team pic"
								/>
								<p>Berat Padilla</p>
							</div>
						</div>
					</div>
					<div className="hrContact flexBox">
						<h3>Contact HR</h3>
						<div className="hrPicColoumn">
							<img
								src={hrManager}
								className="hrManagerPic"
								alt="HR Manager"
							/>
							<p><strong>Amanda Brown</strong></p>
							<p className="hrTitle">HR Management</p>
						</div>
						<div className="hrContactColoumn">
							<div className="contactLine">
								<p><img src={phone} className="phoneIcon" alt="phone"/>+49 157 19571943</p>
							</div>
							<div>
								<p><img src={envelope} className="envelopeIcon" alt="mail"/>amanda.brown@drakon-solutions.com</p>
							</div>
						</div>
					</div>
				</Modal>

				<Navbar bg="dark" variant="dark" className="navbar">
					<Navbar.Brand>
						<img
							alt="logo"
							src={starGazeLogo}
							width="300px"
							className="d-inline-block align-top starmind-logo-suggestion"
						/>
						<div className="rightNav">
							<img
							alt="profile"
							src={profilePic}
							width="350px"
							className="d-inline-block align-top profilePic"
							/>
						</div>
					</Navbar.Brand>
				</Navbar>

				<div className="suggestionBoxWrapper">
					<div className="greets">
						Hey Bob!
						<div>There are suitable positions at <span>Drakon Solutions</span> for you!</div>
					</div>

					{
						this.state.personalSkillMatchingData.map(suggestionBox =>
							<div key={suggestionBox.occupations_conceptURI} className="suggestion-box" onClick={() => this.openOrCloseModal(suggestionBox)}>
								<div className="outerPercentage">
									<div className={this.getProperMatchCSS(suggestionBox.averageTwoWayFit)}>
										<div>{Math.round(suggestionBox.averageTwoWayFit*100)}%</div>
									</div>
								</div>
								<div className="suggestionTexts">
									<div className="suggestion-title">{suggestionBox.occupations_name}</div>
									<div className="suggestion-description">{suggestionBox.occupations_description}</div>
								</div>
							</div>
						)
					}

					<div className="loadMore">
						<img
							alt="chev down"
							src={chevDown}
							width="40px"
							className="d-inline-block align-top chevDown"
						/>
					</div>
				</div>
            </div>
        )
    }
}
