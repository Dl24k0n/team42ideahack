import React from "react";
import './LoginPage.scss';
import stargazeLogo from '../../assets/stargazeLogo.png';
import starmindLogo from '../../assets/starmind-main-logo.svg';
import {Redirect} from "react-router";
import Navbar from 'react-bootstrap/Navbar'

export default class LoginPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            success: '',
            loggedIn: false
        }
        this.updateUsername = this.updateUsername.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
        this.logIn = this.logIn.bind(this);
    }

    updateUsername(e) {
        this.setState({username: e.target.value, success: ''})
    }

    updatePassword(e) {
        this.setState({password: e.target.value, success: ''})
    }

    onKeyUpValue(event) {
        if (event.keyCode === 13) {
            this.logIn()
        }
    }

    logIn() {
        if(this.state.username === "") {
            this.setState({success: 'Please type in a Username'})
        }
        if(this.state.password === "") {
            this.setState({success: 'Please type in a Password'})
        }
        if(this.state.username === "bob@drakon-solutions.com" && this.state.password === "password") {
            this.setState({loggedIn: true})
        } else {
            this.setState({success: 'Wrong username or password. Please try again.'})
        }
    }

    render(){
        if(this.state.loggedIn) {
            return <Redirect push to="/suggestions"/>
        }
        return (
			<>
				<div className="wrapper">
					<Navbar bg="dark" variant="dark" className="navbar">
						<Navbar.Brand href="#">
							<img
								alt=""
								src={starmindLogo}
								width="150"
								height="50"
								className="d-inline-block align-top starmind-logo"
							/>{' '}
						</Navbar.Brand>
					</Navbar>
					<div className="App">
						<div className="panel">
							<img src={stargazeLogo} className="App-logo" alt="logo" />
							<form className="form">
								<div className="form__group field">
									<input type="input" className="form__field" placeholder="Username" name="username" id='username' value={this.state.username} onChange={this.updateUsername} onKeyDown={this.onKeyUpValue.bind(this)} required/>
									<label htmlFor="username" className="form__label">Company Mail</label>
								</div>
								<div className="form__group field">
									<input type="password" className="form__field" placeholder="Password" name="password" id='password' value={this.state.password} onChange={this.updatePassword} onKeyDown={this.onKeyUpValue.bind(this)} required/>
									<label htmlFor="password" className="form__label">Password</label>
								</div>
								<button className='button' type="button" onClick={this.logIn}>
									<div className='line'></div>
									<div className='line'></div>
									<div className='line'></div>
									<div className='line'></div>
									<div className='line'></div>
									<div className='line'></div>
									<span>LOG IN</span>
								</button>
							</form>
							<div className="login-state">{this.state.success}</div>
						</div>
					</div>
				</div>
			</>
        );
    }
}